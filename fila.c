#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "jogo.h"
/* estruturas necessárias para a fila */

// typedef struct nohF {
//     int pedido;
//     struct nohF *prox;    
// } nohF_t;

// typedef struct fila {
//     nohF_t *inicio;
// } fila_t;

void inicializaFila(fila_t *fila) {
    fila->inicio = NULL;
}

int fila_vazia(fila_t *fila) {
    if(fila->inicio == NULL) {
        //está vazia
        return 1;
    }
    else
        //não está vazia
        return 0;
}

nohF_t * cria_nohF() {
    nohF_t *novo;
    novo = (nohF_t *)malloc(sizeof(nohF_t));
    if(!novo) {
        printf("Sem memoria disponivel!\n");
    }
    else   
        novo->prox = NULL;
    return novo;
} 

void insereFila(fila_t *fila, int x) {
    nohF_t *novo = cria_nohF();

    if(fila_vazia(fila)) {
        fila->inicio = novo;
        fila->inicio->pedido = x;      
    }
    else {
        nohF_t *temp;
        temp = fila->inicio;
        while(temp->prox != NULL) {
            temp = temp->prox;
        }
        temp->prox = novo;
        novo->pedido = x;
    }    
}

int remove_fila(fila_t *fila, WINDOW *main_win) {
    if(!fila_vazia(fila)) {       
        nohF_t *temp;
        temp = fila->inicio;
        fila->inicio = fila->inicio->prox;

        //atualiza a janela do ncurses
        //wclear(main_win);
        box(main_win,0,0);
        //mvwprintw(main_win,18, 2,"prox ped at %d",temp->pedido);
        //wrefresh(main_win);         
        return temp->pedido;   
    }
    else {
        return 0;
    }
}

void exibe_fila(fila_t *fila, int l, int pedido_at, WINDOW *main_win) {
    
    mvwprintw(main_win,l+4, 2,"PREPARANDO: %d",pedido_at);
    if(!fila_vazia(fila)){
        nohF_t *aux;
        aux = fila->inicio;
        int i = 8;
        
        while(aux != NULL) {
            //atualiza a janela            
            mvwprintw(main_win,l+5, 2,"FILA: ");    
            mvwprintw(main_win,l+5, i,"%d ",aux->pedido);    
            box(main_win,0,0);        
            i+=2;
            aux = aux->prox;		  		
	    } 
    }    
}

// int main() {
//     fila_t *fila = malloc(sizeof(fila_t));
//     inica_fila(fila);
//     for(int i = 1; i <= 10; i++) {
//         insere(fila, i);
//     }
//     exibe_fila(fila);
//     for(int i = 1; i <= 5; i++) {
//         noh_t *temp = remove_fila(fila);
//         printf("remove: %d\n",temp->pedido);
//     }    
// }