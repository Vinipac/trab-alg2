typedef struct nohP {
    char ingrediente;
    struct nohP *prox; 
    struct nohP *anterior;
} nohP_t;

typedef struct pilha {
    nohP_t *topo;
    nohP_t *base;
    int tam;
} pilha_t;

void inicializaPilha(pilha_t *pilha);
int pilha_vazia(pilha_t  *pilha);
nohP_t *cria_nohP();
void insere_pilha(pilha_t *pilha, char x);
nohP_t* remove_pilha(pilha_t *pilha, WINDOW *win);
void exibe_pilha(pilha_t *pilha, int l, WINDOW *win);