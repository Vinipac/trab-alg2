#include <ncurses.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
// #include "pilha.h"
// #include "fila.h"
#include "jogo.h"

char * X_burger = "PHQP";
char * X_burger_combo = "PHQPF";

char * X_salada = "PHSP";
char * X_salada_combo = "PHSPF";

char * X_tudo = "PQHQSP";
char * X_tudo_combo = "PQHQSPF";

char * Vegetariano = "PSP";
char * Vegetariano_combo = "PSPF";
 

char** leMapa(char * mapa_nome, int *l, int *c)
{   
    FILE *mapa_arq;
    char *linha_leitura = NULL;
    char *temp;
    size_t tam_linha = 0;
    size_t controle;


    mapa_arq = fopen(mapa_nome, "r");
    if (mapa_arq == NULL)
        exit(EXIT_FAILURE);

    /* le a linha */
    controle = getline(&linha_leitura, &tam_linha, mapa_arq);
    *l = atoi(linha_leitura);
    /* le a coluna*/
    controle = getline(&linha_leitura, &tam_linha, mapa_arq);
    *c = atoi(linha_leitura);

    /*aloca uma matriz pro jogo*/
    char** mapa_jogo = malloc((*l)*sizeof(size_t)); // size_t -> pq precisa de endereços maiores
    for(int i = 0; i < (*l); i++) {
        mapa_jogo[i] = (char*)malloc((*c)*sizeof(char));
    }


    int x = 0;
    temp = malloc((*c)*sizeof(char));
    while ((controle = getline(&linha_leitura, &tam_linha, mapa_arq) != -1)) {
        temp = linha_leitura;
        for(int y = 0; y < *c; y++) {            
            mapa_jogo[x][y] = temp[y];            
        }
        x++;      
   }   
   
    free(linha_leitura);
    return mapa_jogo;
}

void imprime_jogo(char** mapa, int l, int c, int x_p, int y_p, WINDOW *mapa_win)
{

    /* troca o lugar do player */
    for(int i = 0; i < l; i++) {
        for(int j = 0; j < c; j++) {
            if(mapa[i][j] == '&') {
                mapa[i][j] = ' ';                               
            }
        }
    }
    
    mapa[y_p][x_p] = '&';
    
    int x,y;
    x = 1;
    y = 1;
    for(int i = 0; i < l; i++) {
        for(int j = 0; j < c; j++) {
            mvwaddch(mapa_win, y, x, mapa[i][j]);
            x++;
        }
        x = 1;
        y++;    
    }    
}
int confere_pedido(int pedido, pilha_t *bandeja, WINDOW *main_win) {
    char ingrediente;
    switch (pedido)
    {
        case 1: // X_burger 
            if(bandeja->tam == 4) {
                for(int i = 3; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja,main_win)->ingrediente;

                    if(ingrediente != X_burger[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
        case 2: // X_salada
            if(bandeja->tam == 4) {
                for(int i = 3; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja, main_win)->ingrediente;
                    if(ingrediente != X_salada[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
        case 3: // X_tudo
            if(bandeja->tam == 6) {
                for(int i = 5; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja, main_win)->ingrediente;
                    if(ingrediente != X_tudo[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
        case 4: // vegetariano
            if(bandeja->tam == 3) {
                for(int i = 2; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja, main_win)->ingrediente;
                    if(ingrediente != Vegetariano[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
        case 5: // X_burger_combo
            if(bandeja->tam == 5) {
                for(int i = 4; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja, main_win)->ingrediente;
                    if(ingrediente != X_burger_combo[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
        case 6: // X_salada_combo
            if(bandeja->tam == 5) {
                for(int i = 4; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja, main_win)->ingrediente;
                    if(ingrediente != X_salada_combo[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
        case 7: // X_tudo_combo
            if(bandeja->tam == 7) {
                for(int i = 6; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja, main_win)->ingrediente;
                    if(ingrediente != X_tudo_combo[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
        case 8: // vegetariano_combo
            if(bandeja->tam == 4) {
                for(int i = 3; i > -1; i--) {
                    ingrediente = remove_pilha(bandeja, main_win)->ingrediente;
                    if(ingrediente != Vegetariano_combo[i])
                        return 0;
                }
                return 1;
            }
            else { 
                return 0;
            }
            break;
    
        default:
            return 0;
            break;
    }
}

void volta_pos(int choice, int* x_p, int* y_p) { //fica na pos atual, ou seja, não move o player
    if((choice == KEY_UP) || (choice == 119))
        ((*y_p)++);               
                
    if((choice == KEY_DOWN) || (choice == 115))
        ((*y_p)--);
            
    if((choice == KEY_LEFT) || (choice == 97)) 
        ((*x_p)++);
                
    if((choice == KEY_RIGHT) || (choice == 100)) 
        (*x_p)--;
}

void exibe_guia(int l, WINDOW * main_win){
    mvwprintw(main_win, l+7,  2,  "GUIA DE PEDIDOS:");
    mvwprintw(main_win, l+9,  2,  "1 : PHQP    (X-Burguer)");
    mvwprintw(main_win, l+10,  2,  "2 : PHSP    (X-Salada)");
    mvwprintw(main_win, l+11, 2,  "3 : PQHQSP  (X-Tudo)");
    mvwprintw(main_win, l+12, 2, "4 : PSP     (Vegetariano)");
    mvwprintw(main_win, l+9,  40, "5 : PHQPF   (Combo X-Burguer)");
    mvwprintw(main_win, l+10,  40, "6 : PHSPF   (Combo X-Salada)");
    mvwprintw(main_win, l+11, 40, "7 : PQHQSPF (Combo X-Tudo)");
    mvwprintw(main_win, l+12, 40, "8 : PSPF    (Combo Vegetariano)");
}

void action(char** mapa, int l, int c, int *x_p, int *y_p, int choice, int *pedido_at, int *acertou_pedido, int *pedidos_entregues, time_t *start_t, time_t *end_t,double *total_t,pilha_t *pilha, fila_t *fila, WINDOW *mapa_win, WINDOW *main_win) {

    if((*x_p) < c && (*y_p) < l && (*x_p) > -1 && (*y_p) > -1) { // pode ir nas paredes
       
 
        int  cel_id = 0;;
        if(mapa[*y_p][*x_p] == '@')
            cel_id = 1;
        if(mapa[*y_p][*x_p] == '#')
            cel_id = 2;
        if(mapa[*y_p][*x_p] == '|' || mapa[*y_p][*x_p] == '-' || mapa[*y_p][*x_p] == '[' || mapa[*y_p][*x_p] == ']')
            cel_id = 3;        
        if(mapa[*y_p][*x_p] == 'H')
            cel_id = 4;
        if(mapa[*y_p][*x_p] == 'P')
            cel_id = 5;
        if(mapa[*y_p][*x_p] == 'S')
            cel_id = 6;
        if(mapa[*y_p][*x_p] == 'Q')
            cel_id = 7;
        if(mapa[*y_p][*x_p] == 'F')
            cel_id = 8;
        if(mapa[*y_p][*x_p] == ' ')
            cel_id = 9;
        switch (cel_id)
        {
            case 1: // '@'
                //verifica e entrega                 
                (*acertou_pedido) = confere_pedido((*pedido_at), pilha, main_win);
                if((*acertou_pedido)) {
                    (*pedido_at) = remove_fila(fila,main_win);
                    wrefresh(main_win);

                    //tempo                    
                    time(end_t);
                    double diff = difftime((*end_t), (*start_t));
                    (*total_t) = (*total_t) + diff;
                    time(start_t);
                    (*pedidos_entregues)++;
                    mvwprintw(main_win, 8 ,c+3,"Tempo de entrega: %.f", diff);
                    wrefresh(main_win); 
                }
                volta_pos(choice, x_p, y_p);
                break;
            case 2: // '#'
                //desempilha
                remove_pilha(pilha,main_win);
                /*fica no lugar anterior*/
                volta_pos(choice, x_p, y_p);
                break;
            case 3: // 'parede'
                /*fica no lugar anterior*/
                volta_pos(choice, x_p, y_p);
                break;
            case 4: // 'H'
                //empilha
                insere_pilha(pilha, mapa[*y_p][*x_p]);
                /*fica no lugar anterior*/
                volta_pos(choice, x_p, y_p);
                break;
            case 5: // 'P'
                //empilha
                insere_pilha(pilha, mapa[*y_p][*x_p]);
                /*fica no lugar anterior*/
                volta_pos(choice, x_p, y_p);
                break;
            case 6: // 'S'
                //empilha
                insere_pilha(pilha, mapa[*y_p][*x_p]);
                /*fica no lugar anterior*/
                volta_pos(choice, x_p, y_p);
                break;
            case 7: // 'Q'
                //empilha
                insere_pilha(pilha, mapa[*y_p][*x_p]);
                /*fica no lugar anterior*/
                volta_pos(choice, x_p, y_p);
                break;
            case 8: // 'F'
                //coloca fritas em "TRUE" ou algo do tipo;
                insere_pilha(pilha, mapa[*y_p][*x_p]);
                /*fica no lugar anterior*/
                volta_pos(choice, x_p, y_p);
                break;
            case 9: // espaço                
                break;        
            default:
                break;
        }

    }   

}

int semPedidos(fila_t *fila) {
    if(fila_vazia(fila)) {
        return 1;
    }
    else
        return 0;
}

void pedido_aleatorio(fila_t *fila, int l, int c,WINDOW *main_win) {
     //coloca a semente do tempo
    int tempo_aleat = rand() % 250;
    int aux = 0;;

    
    if(((tempo_aleat) % 7) == 0 && ((tempo_aleat) % 2 != 0) && (tempo_aleat) % 9 == 0) { //se for múltiplo de 7,2 e 9, insere um numero aleatorio de 0 8;
        aux = rand()%9;
        if(aux == 0) {
            aux = 1;
        }
        insereFila(fila,aux);
        mvwprintw(main_win, 2 ,c+3,"NOVO PEDIDO! : %d",aux);
        wrefresh(main_win);        
    }
}

void tempo(time_t start_t, int c, WINDOW *main_win) { //marca o tempo
    double diff_t;
    time_t atual_t;

    time(&atual_t);
    diff_t = difftime(atual_t, start_t);     
    mvwprintw(main_win, 3 ,c+3,"TEMPO! : %.f  segundos",diff_t);   
}

void jogar(char **mapa, int l, int c, fila_t *fila, pilha_t *bandeja, double *score, WINDOW* mapa_win, WINDOW* main_win) {
    srand(time(NULL));
    int choice; //marca a tecla pressionada
    int pedido_at; //marca o pedido atual;
    int x_p, y_p; //coords do player
    int acertou_pedido = 1; //caso ele erre  
    int pedidos_entregues = 0; //contabiliza os pedidos
    /*tempo*/
    time_t end_t, start_t;
    double total_t;    
    time(&start_t);

    /* ACHA A CORDENADA DO JOGADOR */
    for(int i = 0; i < l; i++) {
        for(int j = 0; j < c; j++) {
            if(mapa[i][j] == '&') {
                x_p = j;
                y_p = i;                
            }
        }
    }  

    /*IMPRESSOES INICIAS*/
    imprime_jogo(mapa, l, c, x_p, y_p, mapa_win);
    pedido_at = remove_fila(fila, main_win);    
    exibe_fila(fila, l, pedido_at, main_win); 
    exibe_guia(l, main_win);
    tempo(start_t, c, main_win);
    mvwprintw(main_win, 6 ,c+3,"SCORE : %.1f",(*score));
    wrefresh(main_win);


    
    
     do
    {       
        choice = wgetch(mapa_win);
        
        //pega o movimento do jogador
        switch(choice)
        {   
            case KEY_UP:
                y_p--;                
                break;
            case KEY_DOWN:
                y_p++;                
                break;
            case KEY_LEFT:
                x_p--;                
                break;            
            case KEY_RIGHT:
                x_p++;              
                break;
            case 119://w
                y_p--;                
                break;
            case 115://s
                y_p++;             
                break;
            case 97://a
                x_p--;                
                break;
            case 100: //d
                x_p++;              
                break;      
                    
            default:
                break;
        }
        /*chamdas de função do jogo*/
        action(mapa, l, c, &x_p, &y_p, choice, &pedido_at, &acertou_pedido, &pedidos_entregues, &start_t, &end_t, &total_t, bandeja, fila, mapa_win, main_win); // pega a intenção do jogador e analisa

        //EXIBE ESTRUTURAS
        exibe_pilha(bandeja, l, main_win);       
        exibe_fila(fila, l, pedido_at, main_win);
        exibe_guia(l, main_win);
        pedido_aleatorio(fila, l, c, main_win);  
        tempo(start_t, c, main_win);

        //CALCULO DO SCORE
        if(total_t != 0) {
            (*score) = (pedidos_entregues/total_t)*100;

        }        
        mvwprintw(main_win, 6 ,c+3,"SCORE : %.1f",(*score));

        //ATUALIZA A TELA
        wrefresh(main_win);
        imprime_jogo(mapa, l, c, x_p, y_p, mapa_win);
        wrefresh(mapa_win);

    }
        while(choice != 27 && acertou_pedido && pedido_at); //se ele acertou o pedido ou acabaram(pedido_at volta 0 quando acabam os pedidos)  
    
    /*imprime tela de fim de jogo*/
    wclear(main_win);
    box(main_win, 0, 0);
    

    if(acertou_pedido) {
         mvwprintw(main_win, 1 ,1,"PARABENS, VOCE VENCEU! SCORE : %.1f",(*score));
         mvwprintw(main_win, 2 ,1,"-PRESSINE QUALQUER TECLA PRA SAIR-");
    }
    else {
        mvwprintw(main_win, 1 ,1,"FIM DE JOGO, VOCE PERDEU! SCORE : %.1f",(*score));
        mvwprintw(main_win, 2 ,1,"-PRESSINE QUALQUER TECLA PRA SAIR-");
    }

    wgetch(main_win);
    endwin();
    exit (EXIT_SUCCESS);
}