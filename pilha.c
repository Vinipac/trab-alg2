#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "jogo.h"

// typedef struct nohP {
//     char ingrediente;
//     struct nohP *prox;    
// } nohP_t;

// typedef struct pilha {
//     nohP_t *topo;
//     int tam;
// } pilha_t;

void inicializaPilha(pilha_t *pilha) {
    pilha->topo = NULL; 
    pilha->base = NULL;
}

int pilha_vazia(pilha_t  *pilha) {
    if(pilha->topo == NULL) 
        return 1;
    
    else
        return 0;
}

nohP_t *cria_nohP() {
    nohP_t *novo = malloc(sizeof(nohP_t));
    novo->prox = NULL;
    return novo;
}

void insere_pilha(pilha_t *pilha, char x) {
    nohP_t *novo = cria_nohP();
    novo->ingrediente = x;
    novo->prox = NULL;
    novo->anterior = NULL;
    if(pilha_vazia(pilha)) {
        pilha->topo = novo;
        pilha->base = novo;
        pilha->tam++;
    }
    else {
        novo->prox = pilha->topo;
        pilha->topo->anterior = novo;
        pilha->topo = novo;
        pilha->tam++;
    }
}

nohP_t* remove_pilha(pilha_t *pilha, WINDOW *main_win) {
    nohP_t *aux = cria_nohP();
    if(!pilha_vazia(pilha)) {
        aux = pilha->topo;        
        pilha->topo = pilha->topo->prox;
        if(pilha->topo != NULL){
            pilha->topo->anterior = NULL;
        }
        pilha->tam--;

        //atualiza a janela do ncurses
         wmove(main_win,12,10);
         wclrtoeol(main_win);
         box(main_win,0,0);
        // wrefresh(main_win);
        return aux;
    }
    else {               
        return NULL;
    }
}

void exibe_pilha(pilha_t *pilha, int l, WINDOW *main_win) {
    if(!pilha_vazia(pilha)) {
        nohP_t *aux = pilha->base;
        int i = 2;
        mvwprintw(main_win,l+3, i, "BANDEJA:",aux->ingrediente);
        i = 10;
        while(aux != NULL) {            
            //atualiza a janela
            mvwprintw(main_win,l+3, i, " %c",aux->ingrediente);    
            box(main_win,0,0);        
            wrefresh(main_win);

             aux = aux->anterior;

            i+=2;
        }
    }
}

// int main() {
//     pilha_t *pilha = malloc(sizeof(pilha_t));
//     inicializaPilha(pilha);
//     for(int i = 1; i <= 10; i++) {
//         insere_pilha(pilha, '*');
//     }
//     exibe_pilha(pilha);
//     noh_t *aux = remove_pilha(pilha);
//     printf("removi: %c\n",aux->ingrediente);
    
  
    
//     return 0;
// }
