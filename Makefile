all: deburger

clean:
	-rm *.out

purge:
	-rm deburger

deburger: deburger.c jogo.c jogo.h fila.c fila.h pilha.c pilha.h
	gcc deburger.c jogo.c pilha.c fila.c -Wall -lncurses -g -o deburger.out
	./deburger.out  