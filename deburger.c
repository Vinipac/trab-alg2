#include <stdio.h>
#include <stdlib.h>
#include "jogo.h"
//só precisa incluir o jogo.h pq ele contém o resto das bibliotecas, se incluisse dnv pilha.h e fila.h daria conflito

int main () {

    /*inicializacao*/    
    int l,c;
    l = c = 0;    
    char **mapa = leMapa("mapa.txt", &l, &c);

    //iniliza estruturas
    pilha_t *bandeja = malloc(sizeof(pilha_t));
    inicializaPilha(bandeja);

    fila_t *fila = malloc(sizeof(fila_t));
    inicializaFila(fila);

    insereFila(fila, 1);
    insereFila(fila, 2);
    insereFila(fila, 3);
    insereFila(fila, 4);
    insereFila(fila, 5);  

    //inicializa o ncurses
    int yMax, xMax;
    initscr();
    cbreak();
    noecho();
    start_color();
    getmaxyx(stdscr, yMax, xMax);

    //cria janelas
    WINDOW *main_win = newwin(yMax,xMax, 0, 0);
    WINDOW *mapa_win = newwin(l+1,c+1, 1, 1);
    WINDOW *side_win = newwin(yMax,xMax, 0, 0);
    
    box(main_win,0,0); //cria uma margem
    box(side_win,1,1);
    refresh();    
    curs_set(0);//desativa o cursor
    wmove(mapa_win, 0, 0);
    wrefresh(side_win);
    wrefresh(mapa_win);
    wrefresh(main_win);
    keypad(mapa_win,true); //habilita keys do teclado
     
    /*para movimentação*/
    double pontos = 0;;

    do {
        jogar(mapa, l, c, fila, bandeja, &pontos, mapa_win, main_win); //precisa passar as janelas do ncurses e l, c
    } while(semPedidos(fila));

//libera a memoria
free(mapa);

return 0;
}