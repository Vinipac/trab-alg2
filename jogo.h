#include <ncurses.h>
#include "fila.h"
#include "pilha.h"
#include <time.h>
char** leMapa(char * mapa_nome, int *l, int *c);
void imprime_jogo(char** mapa, int l, int c, int x_p, int y_p, WINDOW *mapa_win);
int confere_pedido(int pedido, pilha_t *bandeja, WINDOW *main_win);
void volta_pos(int choice, int* x_p, int* y_p);
void action(char** mapa, int l, int c, int *x_p, int *y_p, int choice, int *pedido_at, int *acertou_pedido, int *pedidos_entregues, time_t *start_t, time_t *end_t,double *total_t, pilha_t *pilha, fila_t *fila, WINDOW *mapa_win, WINDOW *main_win);
int semPedidos(fila_t *fila);
void pedido_aleatorio(fila_t *fila, int l, int c, WINDOW *main_win);  
void tempo(time_t start_t, int c, WINDOW *main_win);
void jogar(char **mapa, int l, int c, fila_t *fila, pilha_t *bandeja, double *score, WINDOW* mapa_win, WINDOW* main_win);
void exibe_guia(int l, WINDOW * main_win);




